import Person from '../domain/person.model'
import PersonPersistanceRepository from '../domain/person.persistance.repository'
import CustomError from '../../shared/custom.error'

export default class GetPersonByIdentification {
  private readonly personPersistanceRepository: PersonPersistanceRepository

  constructor(personPersistanceRepository: PersonPersistanceRepository) {
    this.personPersistanceRepository = personPersistanceRepository
  }

  async getPerson (idType: string, idNumber: number): Promise<Person | null> {
    const person = await this.personPersistanceRepository.getUniquePerson(idType, idNumber)
    
    if (!person) {
      throw new CustomError ('PERSON_404', 'Person', 'The person that you want to get was not found')
    }

    return person
  }
}