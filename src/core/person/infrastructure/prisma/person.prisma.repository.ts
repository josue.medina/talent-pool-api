import PersonPersistanceRepository from '../../domain/person.persistance.repository'
import prisma from './prisma.connection'
import Person from '../../domain/person.model'

export default class PersonPrismaRepository implements PersonPersistanceRepository {
  async getAllPersons (): Promise<Person[]> {
    const persons = await prisma.person.findMany({
      select: {
        name: true,
        lastname: true,
        idType: true,
        idNumber: true,
        cityOfBirth: true,
        age: true,
      }
    })
    return persons
  }

  async getPersonById (personId: string): Promise<Person| null> {
    const person = await prisma.person.findUnique({
      where: {
        personId
      }
    })

    return person
  }

  async getUniquePerson (idType: string, idNumber: number): Promise<Person | null> {
    const person = await prisma.person.findFirst({
      where: {
        idNumber,
        idType
      },
      select: {
        personId: true,
        name: true,
        lastname: true,
        idType: true,
        idNumber: true,
        cityOfBirth: true,
        age: true,
      }
    })

    return person
  }

  async getPersonsGreaterOrEqualToAge (age: number): Promise<Person[]> {
    const personsFound = await prisma.person.findMany({
      where: {
        age: {
          gte: age
        }
      },
      select: {
        name: true,
        lastname: true,
        idType: true,
        idNumber: true,
        cityOfBirth: true,
        age: true,
      }
    })
    return personsFound
  }

  async insertPerson ({ personId, name, lastname, age, idType, idNumber, cityOfBirth }: { personId: string, name: string, lastname: string, age: number, idType: string, idNumber: number, cityOfBirth: string }): Promise<Person> {
    const personSaved = await prisma.person.create({
      data: {
        personId,
        name,
        lastname,
        age,
        idType,
        idNumber,
        cityOfBirth
      },
      select: {
        name: true,
        lastname: true,
        idType: true,
        idNumber: true,
        cityOfBirth: true,
        age: true,
      }
    })

    return personSaved
  }

  async updatePersonByIdentification (idType, idNumber, { name, lastname, age, cityOfBirth }: { name: string, lastname: string, age: number, cityOfBirth: string }): Promise<string> {
    await prisma.person.updateMany({
      data: {
        name,
        lastname,
        age,
        cityOfBirth
      },
      where: {
        idType,
        idNumber
      }
    })

    return 'The person was updated'
  }

  async deletePersonByIdentification (idType: string, idNumber: number): Promise<string> {
    await prisma.person.deleteMany({
      where: {
        idNumber,
        idType
      }
    })

    return 'The user has been deleted'
  }
}
