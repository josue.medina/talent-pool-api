import ListAllPersons from '../../src/core/person/application/list.all.persons'
import GetPersonByIdentification from '../../src/core/person/application/get.person.by.identification'
import UpdatePersonByIdentification from '../../src/core/person/application/update.person.by.identification'
import DeletePerson from '../../src/core/person/application/delete.person'
import PersonUuidRepository from '../../src/core/person/infrastructure/uuid/person.uuid.repository'
import SavePerson from '../../src/core/person/application/save.person'
import PersonPrismaRepository from '../../src/core/person/infrastructure/prisma/person.prisma.repository'
import ImageMongooseRepository from '../../src/core/image/infrastructure/mongoose/image.mongoose.repository'
import CustomError from '../../src/core/shared/custom.error'

jest.mock('../../src/core/person/infrastructure/prisma/person.prisma.repository')
jest.mock('../../src/core/image/infrastructure/mongoose/image.mongoose.repository')

describe('Person persistance repository', () => {
  test('The name of the first person saved must be Josue', async () => {
    const personPrismaRepository = new PersonPrismaRepository()
    const spy = jest.spyOn(personPrismaRepository, 'getAllPersons')
    spy.mockReturnValueOnce(Promise.resolve([
      {
        name: 'Josue',
        lastname: 'Medina',
        idType: 'DNI',
        idNumber: 383494043,
        cityOfBirth: 'Peru', 
        age: 21
      },
      {
        name: 'Josue', 
        lastname: 'Medina', 
        idType: 'DNI', 
        idNumber: 383494043, 
        cityOfBirth: 'Peru', 
        age: 21
      },
    ]))

    const listAllPersons = new ListAllPersons(new PersonPrismaRepository())
    const persons = await listAllPersons.list()

    if (!persons) {
      return
    }

    expect(persons).toHaveLength(2)
    expect(persons[0].name).toBe('Josue')
  })

  test('The length of the array of persons must be two', async () => {
    const personPrismaRepository = new PersonPrismaRepository()
    const spy = jest.spyOn(personPrismaRepository, 'getAllPersons')
    spy.mockReturnValueOnce(Promise.resolve([
      {
        name: 'Josue', 
        lastname: 'Medina', 
        idType: 'DNI', 
        idNumber: 383494043, 
        cityOfBirth: 'Peru', 
        age: 21
      },
      {
        name: 'Josue', 
        lastname: 'Medina', 
        idType: 'DNI', 
        idNumber: 383494043, 
        cityOfBirth: 'Peru', 
        age: 21
      },
    ]))

    const listAllPersons = new ListAllPersons(new PersonPrismaRepository())
    const persons = await listAllPersons.list()

    if (!persons) {
      return
    }

    expect(persons).toHaveLength(2)
  })
})

describe('Get unique person', () => {
  test('Get a person successfully', async () => {
    const personPrismaRepository = new PersonPrismaRepository()

    const spy = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    spy.mockReturnValueOnce(Promise.resolve({
      personId: '3und83u9h98jh1920jh09d22',
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383494043, 
      cityOfBirth: 'Peru', 
      age: 21
    }))

    const getPersonByIdentification = new GetPersonByIdentification(personPrismaRepository)
    const person = await getPersonByIdentification.getPerson('DNI', 383494043)

    expect(person?.name).toBe('Josue')
  })

  test('Should throw a error when send credentials that non-exists', async () => {
    const personPrismaRepository = new PersonPrismaRepository()

    const spy = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    spy.mockReturnValue(Promise.resolve(null))

    const getPersonByIdentification = new GetPersonByIdentification(personPrismaRepository) 
    await expect(getPersonByIdentification.getPerson('DNI', 2373798987))
      .rejects.toBeInstanceOf(CustomError)
  })
})

describe('Create a person', () => {
  test('Create a person successfully', async () => {
    const personPrismaRepository =  new PersonPrismaRepository()
    const personUuidRepository = new PersonUuidRepository()

    const spyInsertPerson = jest.spyOn(personPrismaRepository, 'insertPerson')
    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    const spyGeneratePersonId = jest.spyOn(personUuidRepository, 'generatePersonId')

    spyGeneratePersonId.mockReturnValueOnce('i3j983h98d3h8303h33')

    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve(null))

    spyInsertPerson.mockReturnValueOnce(Promise.resolve({
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383434444, 
      cityOfBirth: 'Peru', 
      age: 21
    }))

    const savePerson = new SavePerson(personUuidRepository, personPrismaRepository)
    const person = await savePerson.save('Josue', 'Medina', 21, 'DNI', 383434444, 'Peru')
    expect(person?.name).toBe('Josue')
  })

  test('Create a person that id number is already exists', async () => {
    const personPrismaRepository =  new PersonPrismaRepository()
    const personUuidRepository = new PersonUuidRepository()

    const spyInsertPerson = jest.spyOn(personPrismaRepository, 'insertPerson')
    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    const spyGeneratePersonId = jest.spyOn(personUuidRepository, 'generatePersonId')

    spyGeneratePersonId.mockReturnValueOnce('i3j983h98d3h8303h33')

    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve({
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383434444, 
      cityOfBirth: 'Peru', 
      age: 21
    }))

    spyInsertPerson.mockReturnValueOnce(Promise.resolve({
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383434444, 
      cityOfBirth: 'Peru', 
      age: 21
    }))

    const savePerson = new SavePerson(personUuidRepository, personPrismaRepository)
    
    await expect(savePerson.save('Josue', 'Medina', 21, 'DNI', 383434444, 'Peru'))
      .rejects.toBeInstanceOf(CustomError)
  })
})

describe('Update a person', () => {
  test('Update a person successfully', async () => {
    const personPrismaRepository = new PersonPrismaRepository()

    const spyUpdatePersonByIdentification = jest.spyOn(personPrismaRepository, 'updatePersonByIdentification')
    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve({
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383494043, 
      cityOfBirth: 'Peru', 
      age: 21
    }))
    spyUpdatePersonByIdentification.mockReturnValueOnce(Promise.resolve('The person was updated'))

    const updatePersonByIdentification = new UpdatePersonByIdentification(personPrismaRepository)
    await expect(updatePersonByIdentification.update('DNI', 383494043, {
      name: 'Enzo',
      lastname: 'Villanueva',
      age: 20,
      cityOfBirth: 'Peru'
    }))
      .resolves.toBe('The person was updated')
  })

  test('Must throw an error when try to update a person that non-exists', async () => {
    const personPrismaRepository = new PersonPrismaRepository()

    const spyUpdatePersonByIdentification = jest.spyOn(personPrismaRepository, 'updatePersonByIdentification')
    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve(null))
    spyUpdatePersonByIdentification.mockReturnValueOnce(Promise.resolve('The person was updated'))

    const updatePersonByIdentification = new UpdatePersonByIdentification(personPrismaRepository)
    await expect(updatePersonByIdentification.update('DNI', 383494044, {
      name: 'Enzo',
      lastname: 'Villanueva',
      age: 20,
      cityOfBirth: 'Peru'
    }))
      .rejects.toBeInstanceOf(CustomError)
  })
})

describe('Delete a person', () => {
  test('Delete a person successfully', async () => {
    const personPrismaRepository = new PersonPrismaRepository()
    const imageMongooseRepository = new ImageMongooseRepository()

    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    const spyDeletePersonByIdentification = jest.spyOn(personPrismaRepository, 'deletePersonByIdentification')
    const spyDeleteImagesByPersonId = jest.spyOn(imageMongooseRepository, 'deleteImagesByPersonId')

    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383494043, 
      cityOfBirth: 'Peru', 
      age: 21
    }))
    spyDeleteImagesByPersonId.mockReturnValueOnce(Promise.resolve())
    spyDeletePersonByIdentification.mockReturnValueOnce(Promise.resolve('The user has been deleted'))

    const deletePerson = new DeletePerson(personPrismaRepository, imageMongooseRepository)
    await expect(deletePerson.delete('DNI', 383494043))
      .resolves.toBe('The user has been deleted')
  })

  test('Delete a person that id number non-exists', async () => {
    const personPrismaRepository = new PersonPrismaRepository()
    const imageMongooseRepository = new ImageMongooseRepository()

    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    const spyDeletePersonByIdentification = jest.spyOn(personPrismaRepository, 'deletePersonByIdentification')
    const spyDeleteImagesByPersonId = jest.spyOn(imageMongooseRepository, 'deleteImagesByPersonId')

    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve(null))
    spyDeleteImagesByPersonId.mockReturnValueOnce(Promise.resolve())
    spyDeletePersonByIdentification.mockReturnValueOnce(Promise.resolve('The user has been deleted'))

    const deletePerson = new DeletePerson(personPrismaRepository, imageMongooseRepository)
    await expect(deletePerson.delete('DNI', 2888233))
      .rejects.toBeInstanceOf(CustomError)
  })
})