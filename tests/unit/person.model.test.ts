import Person from '../../src/core/person/domain/person.model'
import PersonUuidRepository from '../../src/core/person/infrastructure/uuid/person.uuid.repository'

const mockPersonUuidRepository: jest.Mocked<PersonUuidRepository> = {
  generatePersonId: jest.fn(() => '93h983h89r98h398')
}

describe('Person model', () => {
  test('Should save a person with a generated id', async () => {  
    const person = new Person({
      personIdGeneratorRepository: mockPersonUuidRepository, 
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383494043, 
      cityOfBirth: 'Peru', 
      age: 21
    })

    expect(person.personId).toBe('93h983h89r98h398')
  })
})
