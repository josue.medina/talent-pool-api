import Image from '../../src/core/image/domain/image.model'
import ImageUuidRepository from '../../src/core/image/infrastructure/uuid/image.uuid.repository'

const mockImageUuidRepository: jest.Mocked<ImageUuidRepository> = {
  generateImageId: jest.fn(() => '93h983h89r98h398')
}

describe('Image model', () => {
  test('Should save an image with a generated id', async () => {  
    const image = new Image({
      imageIdGeneratorRepository: mockImageUuidRepository,
      personId: 'ansdjas90d9ajs',
      url: 'http://www.google.com',
      title: 'Title',
      description: 'Description'
    })

    expect(image.imageId).toBe('93h983h89r98h398')
  })
})
