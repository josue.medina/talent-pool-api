import ListAllImages from '../../src/core/image/application/list.all.images'
import GetUniqueImage from '../../src/core/image/application/get.unique.image'
import UpdateImage from '../../src/core/image/application/update.image'
import DeleteImage from '../../src/core/image/application/delete.image'
import UploadImage from '../../src/core/image/application/upload.image.by.person'
import ImageMongooseRepository from '../../src/core/image/infrastructure/mongoose/image.mongoose.repository'
import ImageUuidRepository from '../../src/core/image/infrastructure/uuid/image.uuid.repository'
import ImageAwsRepository from '../../src/core/image/infrastructure/aws/image.aws.repository'
import PersonPrismaRepository from '../../src/core/person/infrastructure/prisma/person.prisma.repository'
import CustomError from '../../src/core/shared/custom.error'

jest.mock('../../src/core/image/infrastructure/mongoose/image.mongoose.repository')

describe('Image persistance repository', () => {
  test('The title of the first image saved must be Test image', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()
    const spy = jest.spyOn(imageMongooseRepository, 'getAllImages')
    spy.mockReturnValueOnce(Promise.resolve([
      {
        _id: '6351694abf44a241b494bc34',
        imageId: '59b64f36-38bb-4e74-b57f-fc5c55d1bfc4',
        url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg',
        title: 'Test image',
        description: 'Test description'
      },
      {
        _id: '6351694abf44a241b494bc33',
        imageId: '59b64f36-38bb-4e74-b57f-fc5c55d1bfc5',
        url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg',
        title: 'Test image 2',
        description: 'Test description 2'
      }
    ]))

    const listAllImages = new ListAllImages(imageMongooseRepository)
    const images = await listAllImages.list()

    if (!images) {
      return
    }

    expect(images[0].title).toBe('Test image')
  })

  test('The length of the array of images must be two', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()
    const spy = jest.spyOn(imageMongooseRepository, 'getAllImages')
    spy.mockReturnValueOnce(Promise.resolve([
      {
        _id: '6351694abf44a241b494bc34',
        imageId: '59b64f36-38bb-4e74-b57f-fc5c55d1bfc4',
        url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg',
        title: 'Test image',
        description: 'Test description'
      },
      {
        _id: '6351694abf44a241b494bc33',
        imageId: '59b64f36-38bb-4e74-b57f-fc5c55d1bfc5',
        url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg',
        title: 'Test image 2',
        description: 'Test description 2'
      }
    ]))

    const listAllImages = new ListAllImages(new ImageMongooseRepository())
    const images = await listAllImages.list()

    if (!images) {
      return
    }

    expect(images).toHaveLength(2)
  })
})

describe('Get unique image', () => {
  test('Get an unique image by image id successfully', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()
    const personPrismaRepository = new PersonPrismaRepository()

    const spyGetPersonById = jest.spyOn(personPrismaRepository, 'getPersonById')
    const spyGetImageById = jest.spyOn(imageMongooseRepository, 'getImageById')

    spyGetPersonById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383494043, 
      cityOfBirth: 'Peru', 
      age: 21
    }))

    spyGetImageById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test',
      description: 'Test',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const getUniqueImage = new GetUniqueImage(imageMongooseRepository, personPrismaRepository)
    const image = await getUniqueImage.getImage('2j029ejh0982h022')
    expect(image?.title).toBe('Image test')
  })

  test('Should throw an error when try to get an unique image by image id sending a person id that non-exists', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()
    const personPrismaRepository = new PersonPrismaRepository()

    const spyGetPersonById = jest.spyOn(personPrismaRepository, 'getPersonById')
    const spyGetImageById = jest.spyOn(imageMongooseRepository, 'getImageById')

    spyGetPersonById.mockReturnValueOnce(Promise.resolve(null))

    spyGetImageById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test',
      description: 'Test',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const getUniqueImage = new GetUniqueImage(imageMongooseRepository, personPrismaRepository)
    await expect(getUniqueImage.getImage('2j029ejh0982h022')).rejects.toBeInstanceOf(CustomError)
  })
})

describe('Upload an image', () => {
  test('Upload an image successfully', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()
    const personPrismaRepository = new PersonPrismaRepository()
    const imageAwsRepository = new ImageAwsRepository()
    const imageUuidRepository = new ImageUuidRepository()

    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    const spyUploadImage = jest.spyOn(imageAwsRepository, 'uploadImage')
    const spyGenerateImageId = jest.spyOn(imageUuidRepository, 'generateImageId')
    const spySaveImageByPersonId = jest.spyOn(imageMongooseRepository, 'saveImageByPersonId')

    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      name: 'Josue', 
      lastname: 'Medina', 
      idType: 'DNI', 
      idNumber: 383494043, 
      cityOfBirth: 'Peru', 
      age: 21
    }))
    spyUploadImage.mockReturnValueOnce(Promise.resolve('https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'))
    spyGenerateImageId.mockReturnValueOnce('asnd89qh2893he22')
    spySaveImageByPersonId.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test',
      description: 'Test',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const uploadImage = new UploadImage(imageUuidRepository, imageMongooseRepository, personPrismaRepository, imageAwsRepository)
    const image = await uploadImage.upload({ 
      path: `${__dirname}/images/carnet.png`, 
      fileName: 'carnet.png', 
      idType: 'DNI', 
      idNumber: 383494043, 
      title: 'Image test', 
      description: 'Test', 
      isUnlinkeable: true 
    })

    expect(image?.title).toBe('Image test')
  })

  test('Should throw an error when tries to upload an image that person id non-exists', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()
    const personPrismaRepository = new PersonPrismaRepository()
    const imageAwsRepository = new ImageAwsRepository()
    const imageUuidRepository = new ImageUuidRepository()

    const spyGetUniquePerson = jest.spyOn(personPrismaRepository, 'getUniquePerson')
    const spyUploadImage = jest.spyOn(imageAwsRepository, 'uploadImage')
    const spyGenerateImageId = jest.spyOn(imageUuidRepository, 'generateImageId')
    const spySaveImageByPersonId = jest.spyOn(imageMongooseRepository, 'saveImageByPersonId')

    spyGetUniquePerson.mockReturnValueOnce(Promise.resolve(null))
    spyUploadImage.mockReturnValueOnce(Promise.resolve('https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'))
    spyGenerateImageId.mockReturnValueOnce('asnd89qh2893he22')
    spySaveImageByPersonId.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test',
      description: 'Test',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const uploadImage = new UploadImage(imageUuidRepository, imageMongooseRepository, personPrismaRepository, imageAwsRepository)

    await expect(uploadImage.upload({ 
      path: `${__dirname}/images/carnet.png`, 
      fileName: 'carnet.png', 
      idType: 'DNI', 
      idNumber: 383494043, 
      title: 'Image test', 
      description: 'Test', 
      isUnlinkeable: true 
    }))
      .rejects.toBeInstanceOf(CustomError)
  })
})

describe('Update an image', () => {
  test('Update an image by image id successfully', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()

    const spyGetImageById = jest.spyOn(imageMongooseRepository, 'getImageById')
    const spyUpdateImageById = jest.spyOn(imageMongooseRepository, 'updateImageById')

    spyGetImageById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test',
      description: 'Test',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    spyUpdateImageById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test updated',
      description: 'Test updated',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const updateImage = new UpdateImage(imageMongooseRepository)
    const image = await updateImage.update({ 
      imageId: '2j029ejh0982h022', 
      title: 'Image test updated', 
      description: 'Test updated' 
    })
    
    expect(image?.title).toBe('Image test updated')
  })

  test('Should throw an error when tries to update an image that image id non-exists', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()

    const spyGetImageById = jest.spyOn(imageMongooseRepository, 'getImageById')
    const spyUpdateImageById = jest.spyOn(imageMongooseRepository, 'updateImageById')

    spyGetImageById.mockReturnValueOnce(Promise.resolve(null))

    spyUpdateImageById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test updated',
      description: 'Test updated',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const updateImage = new UpdateImage(imageMongooseRepository)
    
    await expect(updateImage.update({ 
      imageId: '2j029ejh0982h022', 
      title: 'Image test updated', 
      description: 'Test updated' 
    }))
      .rejects.toBeInstanceOf(CustomError)
  })
})

describe('Delete an image', () => {
  test('Delete an image by image id successfully', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()

    const spyDeleteImageById = jest.spyOn(imageMongooseRepository, 'deleteImageById')

    spyDeleteImageById.mockReturnValueOnce(Promise.resolve({
      personId: '2nd029j292',
      title: 'Image test updated',
      description: 'Test updated',
      imageId: '2j029ejh0982h022',
      url: 'https://talent-pool-images.s3.us-west-2.amazonaws.com/60158c4f-c579-44f3-b5ee-be1971102bef.jpg'
    }))

    const deleteImage = new DeleteImage(imageMongooseRepository)
    const image = await deleteImage.delete('ausdh88209j92d')

    expect(image?.title).toBe('Image test updated')
  })

  test('Should throw an error when tries to delete an image sending an image id that non-exists', async () => {
    const imageMongooseRepository = new ImageMongooseRepository()

    const spyDeleteImageById = jest.spyOn(imageMongooseRepository, 'deleteImageById')

    spyDeleteImageById.mockReturnValueOnce(Promise.resolve(null))

    const deleteImage = new DeleteImage(imageMongooseRepository)
    await expect(deleteImage.delete('ausdh88209j92d'))
      .rejects.toBeInstanceOf(CustomError)
  })
})